#include "main.h"

#include <cctype>
#include <charconv>
#include <cstring>
#include <random>

#include "SAMP/Library.h"
#include "callfunc.hpp"

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	if ( SAMP::isR1() ) {
		strlen_.changeAddr( 0x6390E );
		shadowFont.changeAddr( 0x638B8 );
	} else if ( SAMP::isR2() ) {
		strlen_.changeAddr( 0x639DE );
		shadowFont.changeAddr( 0x63988 );
	} else if ( SAMP::isR4() ) {
		strlen_.changeAddr( 0x6749E );
		shadowFont.changeAddr( 0x67448 );
	} else if ( SAMP::isDL() ) {
		strlen_.changeAddr( 0x66F4E );
		shadowFont.changeAddr( 0x66EF8 );
	}

	strlen_.onAfter += std::tuple{ this, &AsiPlugin::strlen };
	strlen_.install( 0, 0, false );

	shadowFont.enable();

	std::random_device r;
	std::default_random_engine e1( r() );
	std::uniform_int_distribution<std::uint8_t> channel( 0x10, 0x40 );
	fallback_color = channel( e1 ) | ( channel( e1 ) << 8 ) | ( channel( e1 ) << 16 );
}

AsiPlugin::~AsiPlugin() {
	shadowFont.disable();
	if ( base != nullptr ) SAMP::Base::DeleteInstance();
}

void AsiPlugin::strlen( SRHook::CPU &cpu ) {
	auto text = std::string_view( reinterpret_cast<char *>( cpu.EDI ) );

	if ( base == nullptr ) base = SAMP::Base::Instance();

	if ( base->Pools() == nullptr ) return;

	auto pool = base->Pools()->PlayerPool();
	if ( pool == nullptr ) return;

	int color = 0xFFFFFFFF;
	int def_color = 0xFF'000000;
	switch ( SAMP::Version() ) {
		case SAMP::eVerCode::R1:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3D90 );
			def_color = *reinterpret_cast<int *>( SAMP::Library() + 0x6392F );
			break;
		case SAMP::eVerCode::R2:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3DA0 );
			def_color = *reinterpret_cast<int *>( SAMP::Library() + 0x639FF );
			break;
		case SAMP::eVerCode::R3:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3DA0 );
			def_color = *reinterpret_cast<int *>( SAMP::Library() + 0x66D7F );
			break;
		case SAMP::eVerCode::R4:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3F10 );
			def_color = *reinterpret_cast<int *>( SAMP::Library() + 0x674BF );
			break;
		case SAMP::eVerCode::DL:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3E20 );
			def_color = *reinterpret_cast<int *>( SAMP::Library() + 0x66F6F );
			break;
		default:
			break;
	}
	color = 0x00FFFFFF - ( color & 0x00FFFFFF );
	if ( ( color & 0xFF ) < 0x10 && ( ( color >> 8 ) & 0xFF ) < 0x10 && ( ( color >> 16 ) & 0xFF ) < 0x10 ) color = fallback_color;
	char szColor[kColorTagLength + 1]{ '\0' };
	snprintf( szColor, sizeof( szColor ), "{%06X}", color );

	def_color = def_color & 0x00FFFFFF;
	char szDefaultColor[kColorTagLength + 1]{ '\0' };
	snprintf( szDefaultColor, sizeof( szDefaultColor ), "{%06X}", def_color );

	bool nick_highlighted = false;
	auto nick = pool->localPlayerNick();
	auto string = text;
	while ( true ) {
		auto at = string.rfind( nick );
		if ( at == std::string::npos ) break;

		highlight_text( text, at, nick.length(), szColor, szDefaultColor );
		nick_highlighted = true;

		string = string.substr( 0, at );
	}

	text = text.data();
	if ( nick_highlighted ) return;

	char id[8]{ '\0' };
	auto [ptr, ec] = std::to_chars( id, id + sizeof( id ), pool->localPlayerId(), 10 );
	if ( ec != std::errc() ) return;

	auto id_pos = text.find( id );
	if ( id_pos == std::string::npos ) return;

	if ( id_pos != 0 ) {
		if ( std::isalnum( text[id_pos - 1] ) ) return;
		if ( std::ispunct( text[id_pos - 1] ) ) return;
	}

	auto id_len = ::std::strlen( id );
	if ( std::isalnum( text[id_pos + id_len] ) ) return;
	if ( std::ispunct( text[id_pos + id_len] ) && text[id_pos + id_len] != ',' ) return;

	highlight_text( text, id_pos, id_len, szColor, szDefaultColor );
}

void AsiPlugin::highlight_text( std::string_view string,
								int highlight_at,
								int highlight_length,
								std::string_view higlight_color_tag,
								std::string_view default_color_tag ) {

	auto length = ::std::strlen( string.data() );
	for ( int i = length; i >= static_cast<int>( highlight_at + highlight_length ); --i ) {
		auto text_ = const_cast<char *>( string.data() );
		text_[i + kColorTagLength * 2] = text_[i];
	}
	for ( int i = highlight_at + highlight_length + 1; i >= static_cast<int>( highlight_at ); --i ) {
		auto text_ = const_cast<char *>( string.data() );
		text_[i + kColorTagLength] = text_[i];
	}

	// Neon
	std::memcpy( const_cast<char *>( &string[highlight_at] ), higlight_color_tag.data(), higlight_color_tag.length() );

	// Restore shadow color
	std::memcpy( const_cast<char *>( &string[highlight_at + kColorTagLength + highlight_length] ),
				 default_color_tag.data(),
				 default_color_tag.length() );
}
