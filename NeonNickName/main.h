#ifndef MAIN_H
#define MAIN_H

#include "SAMP/Base.h"
#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

#include <string_view>

#include "SRHook.hpp"
#include "Patch.h"
#include "samp.hpp"

class AsiPlugin : public SRDescent {
	static constexpr auto kColorTagLength = 8;

	SRHook::Hook<> strlen_{ 0x66D5E, 7, "samp" };
	SRHook::Patch shadowFont{ 0x66D08, { 0x00 }, "samp" };

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	int fallback_color = -1;
	SAMP::Base *base = nullptr;
	void strlen( SRHook::CPU &cpu );

	void highlight_text( std::string_view string,
						 int highlight_at,
						 int highlight_length,
						 std::string_view higlight_color_tag,
						 std::string_view default_color_tag = "{000000}" );
};

#endif // MAIN_H
